package com.mastercontrol;

import lombok.Data;

import java.util.UUID;

/**
 * This is my java doc.
 */
@Data
public class DoCoolStuff {
    private double myFace;
    private String firstName;
    private String lastName;
    private UUID thisIsSpecial;

    /**
     * This is a function.
     * @return the uuid that this object has.
     */
    public UUID iLikePizza() {
        System.out.println("Hello Now!");

        Double great = ++myFace;

        myFace = great;

        return thisIsSpecial;
    }
}
